networkparameters
=========

This role configures and disables several network-protocols and parameters. It's based on CIS Red Hat Enterprise Linux Benchmark

Requirements
------------

None

Role Variables
--------------

enable_networkparameters: true - [controls if the module will run at all]

disable_ip_forwarding: true - [whether to disable ip forwarding]

disable_send_packet_redirects: true - [whether to disable packet redirects]

disable_ipv6: true - [whether to disable ipv6]

enable_ipv6_lo: true - [whether to enable ipv6 for loopback interface]

set_keepalive: true - [whether to set keepalive]

Dependencies
------------

None

License
-------

MIT

Author Information
------------------

signits@acme.dev
